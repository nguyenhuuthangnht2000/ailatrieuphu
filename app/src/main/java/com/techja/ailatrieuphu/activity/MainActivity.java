package com.techja.ailatrieuphu.activity;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.techja.ailatrieuphu.App;
import com.techja.ailatrieuphu.MediaManager;
import com.techja.ailatrieuphu.OnActionCallBack;
import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.base.BaseActivity;
import com.techja.ailatrieuphu.databases.entities.Question;
import com.techja.ailatrieuphu.databinding.ActivityMainBinding;
import com.techja.ailatrieuphu.fragment.M001MainFrg;
import com.techja.ailatrieuphu.viewmodel.MainViewModel;

import java.util.List;


public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> {
    private static final String TAG = MainActivity.class.getName();

    @Override
    protected void initViews() {

        initDB();
    }

    @Override
    protected void onStop() {

        super.onStop();
        MediaManager.getInstance().pauseSong();
    }

    @Override
    protected void onStart() {
        super.onStart();
        MediaManager.getInstance().playSong();
    }

    private void initDB() {
        new Thread() {
            @Override
            public void run() {
                try {
                    List<Question> listQuestion = App.getInstance().getDb().getDAO().getAll();
                    Log.i(TAG, "ListQuestion " + listQuestion.size());
                    runOnUI((data, key) -> goToMainScreen(listQuestion));
                } catch (Exception e) {
                    runOnUI((data, key) -> {
                        Log.i(TAG, "" + e);
                        showALert();
                    });
                }
            }
        }.start();
    }


    private void goToMainScreen(List<Question> listQuestion) {
        App.getInstance().getStorage().setListQuestion(listQuestion);
        new Handler().postDelayed(() -> {
            mBinding.ivLogo.setVisibility(View.GONE);
            mBinding.progressBarLoading.setVisibility(View.GONE);
            showFrg(M001MainFrg.TAG);
        }, 2000);
    }

    private void showALert() {
        Toast.makeText(this, "Không lấy được dữ liệu", Toast.LENGTH_LONG).show();
    }

    @Override
    protected ActivityMainBinding initViewBinding(View view) {
        return ActivityMainBinding.bind(view);
    }

    @Override
    protected int getLayOutId() {
        return R.layout.activity_main;
    }

    @Override
    protected Class<MainViewModel> initViewModel() {
        return MainViewModel.class;
    }

    public void runOnUI(OnActionCallBack callBack) {
        runOnUiThread(() -> callBack.callBack(null, null));
    }
}