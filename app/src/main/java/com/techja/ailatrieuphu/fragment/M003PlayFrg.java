package com.techja.ailatrieuphu.fragment;

import android.view.View;

import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.base.BaseFragment;
import com.techja.ailatrieuphu.databinding.FrgM003PlayBinding;
import com.techja.ailatrieuphu.viewmodel.M003ViewModel;

public class M003PlayFrg extends BaseFragment<FrgM003PlayBinding, M003ViewModel> {
    public static final String TAG = M003PlayFrg.class.getName();

    @Override
    protected void initViews() {

    }

    @Override
    protected void clickView(View view) {

    }

    @Override
    protected Class<M003ViewModel> initViewModel() {
        return M003ViewModel.class;
    }

    @Override
    protected FrgM003PlayBinding initViewBinding(View view) {
        return FrgM003PlayBinding.bind(view);
    }

    @Override
    protected int getLayOutId() {
        return R.layout.frg_m003_play;
    }
}
