package com.techja.ailatrieuphu.fragment;


import android.media.MediaPlayer;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.techja.ailatrieuphu.MediaManager;
import com.techja.ailatrieuphu.OnActionCallBack;
import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.activity.MainActivity;
import com.techja.ailatrieuphu.base.BaseFragment;
import com.techja.ailatrieuphu.databinding.FrgM002RuleBinding;
import com.techja.ailatrieuphu.dialog.InfoReadyDialog;
import com.techja.ailatrieuphu.viewmodel.M002ViewModel;

public class M002RuleFrg extends BaseFragment<FrgM002RuleBinding, M002ViewModel> {
    public static final String TAG = M002RuleFrg.class.getName();

    @Override
    protected void initViews() {
        MediaManager.getInstance().playGame(R.raw.song_rule, new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                MediaManager.getInstance().playGame(R.raw.song_ready, new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer1) {
                        M002RuleFrg.this.showReadyDialog();
                    }
                });
            }
        });
        mBinding.milestone.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.slide_left));
        mBinding.btHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaManager.getInstance().stopPlayGame();
                // M002RuleFrg.this.showReadyDialog();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MainActivity act = (MainActivity) mContext;
                        act.showFrg(M003PlayFrg.TAG);
                    }
                }, 5000);
            }
        });

    }


    private void showReadyDialog() {
        InfoReadyDialog inform = new InfoReadyDialog(mContext, new OnActionCallBack() {
            @Override
            public void callBack(Object data, String key) {
                if (key.equals(InfoReadyDialog.KEY_BACK)) {
                    doBack();
                } else if (key.equals(InfoReadyDialog.KEY_READY)) {
                    doReady();
                }
            }
        });
        inform.show();
    }

    private void doReady() {
        MediaManager.getInstance().stopPlayGame();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MainActivity act = (MainActivity) mContext;
                act.showFrg(M003PlayFrg.TAG);
            }
        }, 5000);
    }


    private void doBack() {
        MediaManager.getInstance().stopPlayGame();
        MainActivity act = (MainActivity) mContext;
        act.onBackPressed();
    }

    @Override
    protected void clickView(View view) {

    }

    @Override
    protected Class<M002ViewModel> initViewModel() {
        return M002ViewModel.class;
    }

    @Override
    protected FrgM002RuleBinding initViewBinding(View view) {
        return FrgM002RuleBinding.bind(view);
    }

    @Override
    protected int getLayOutId() {
        return R.layout.frg_m002_rule;
    }
}
