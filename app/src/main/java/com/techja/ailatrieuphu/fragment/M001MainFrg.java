package com.techja.ailatrieuphu.fragment;

import android.view.View;

import com.techja.ailatrieuphu.MediaManager;
import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.activity.MainActivity;
import com.techja.ailatrieuphu.base.BaseFragment;
import com.techja.ailatrieuphu.databinding.FrgM001MainBinding;
import com.techja.ailatrieuphu.viewmodel.M001ViewModel;

public class M001MainFrg extends BaseFragment<FrgM001MainBinding, M001ViewModel> {
    public static final String TAG = M001MainFrg.class.getName();

    @Override
    protected void initViews() {
        MediaManager.getInstance().playBG(R.raw.song_intro);
        mBinding.ivPlay.setOnClickListener(this);
        mBinding.ivInfo.setOnClickListener(this);
        mBinding.ivCup.setOnClickListener(this);
        mBinding.ivSetting.setOnClickListener(this);
    }


    @Override
    protected void clickView(View view) {
        if (view.getId() == R.id.iv_play) {
            MediaManager.getInstance().stopBG();
            MainActivity act = (MainActivity) mContext;
            act.showFrg(M002RuleFrg.TAG);
        }
    }

    @Override
    protected Class<M001ViewModel> initViewModel() {
        return M001ViewModel.class;
    }

    @Override
    protected FrgM001MainBinding initViewBinding(View view) {
        return FrgM001MainBinding.bind(view);
    }

    @Override
    protected int getLayOutId() {
        return R.layout.frg_m001_main;
    }
}
