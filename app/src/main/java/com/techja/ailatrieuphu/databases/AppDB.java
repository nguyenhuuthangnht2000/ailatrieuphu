package com.techja.ailatrieuphu.databases;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.techja.ailatrieuphu.databases.dao.QuestionDAO;
import com.techja.ailatrieuphu.databases.entities.Question;

@Database(entities = {Question.class}, version = 1)
public abstract class AppDB extends RoomDatabase {
    public abstract QuestionDAO getDAO();
}
