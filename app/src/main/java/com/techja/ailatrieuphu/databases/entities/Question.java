package com.techja.ailatrieuphu.databases.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "Question", primaryKeys = "_id")
public class Question {
    @ColumnInfo(name = "_id")
    @NonNull
    public int id;
    @ColumnInfo(name = "question")
    public String question;
    @ColumnInfo(name = "casea")
    public String caseA;
    @ColumnInfo(name = "caseb")
    public String caseB;
    @ColumnInfo(name = "casec")
    public String caseC;
    @ColumnInfo(name = "cased")
    public String caseD;
    @ColumnInfo(name = "truecase")
    @NonNull
    public int truecase;
    @ColumnInfo(name = "level")
    @NonNull
    public int level;

    public Question() {
    }

    public Question(int id, String question, String caseA, String caseB, String caseC, String caseD, int truecase, int level) {
        this.id = id;
        this.question = question;
        this.caseA = caseA;
        this.caseB = caseB;
        this.caseC = caseC;
        this.caseD = caseD;
        this.truecase = truecase;
        this.level = level;
    }

    @Override
    public String toString() {
        return "Question{" +
                "question='" + question + '\'' +
                "id=" + id + '\'' +
                ", level=" + level +
                ", caseA='" + caseA + '\'' +
                ", caseB='" + caseB + '\'' +
                ", caseC='" + caseC + '\'' +
                ", caseD='" + caseD + '\'' +
                ", truecase=" + truecase +
                '}';
    }
}

