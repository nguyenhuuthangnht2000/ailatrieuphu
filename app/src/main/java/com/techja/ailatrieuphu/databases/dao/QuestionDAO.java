package com.techja.ailatrieuphu.databases.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.techja.ailatrieuphu.databases.entities.Question;

import java.util.List;

@Dao
public interface QuestionDAO {
    @Query("SELECT* FROM (SELECT * FROM Question WHERE level =1 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =2 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =3 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =4 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =5 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =6 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =7 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =8 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =9 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =10 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =11 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =13 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =14 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM Question WHERE level =15 ORDER by random() LIMIT 1)\n" +
            "ORDER by level ASC")
    List<Question> getAll();

}
